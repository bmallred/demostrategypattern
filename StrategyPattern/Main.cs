using System;
using System.Collections.Generic;

namespace StrategyPattern
{
	/// <summary>
	/// Main class.
	/// </summary>
	/// <remarks>
	/// <para>This is an example of the strategy pattern in use. While this may be an over simplification of the standard use the pattern can be more easily observed and recognized.</para>
	/// </remarks>
	public class MainClass
	{
		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		/// <param name='args'>
		/// The command-line arguments.
		/// </param>
		public static void Main (string[] args)
		{
			// Here we will define a list of strategies to be applied based upon the key.
			// In this example each strategy applies the same action, but in a real scenario
			// the actions would all be different (otherwise this makes no sense).
			var strategies = new Dictionary<string, Action<int, string>>()
			{
				{ "one", (i, s) => Console.WriteLine("{0} \"{1}\" beer", i, s) },
				{ "two", (i, s) => Console.WriteLine("{0} \"{1}\" beer", i, s) },
				{ "three", (i, s) => Console.WriteLine("{0} \"{1}\" beer", i, s) },
				{ "four", (i, s) => Console.WriteLine("{0} \"{1}\" beer... time to give away the keys!", i, s) },
				{ "five", (i, s) => Console.WriteLine("{0} \"{1}\" beer", i, s) },
				{ "six", (i, s) => Console.WriteLine("{0} \"{1}\" beer... off to get more!", i, s) },
			};

			// Declare some items that will require strategies to be applied.
			var exampleActions = new List<string>()
			{
				"one",
				"two",
				"four"
			};

			// Loop through each example item and apply the appropriate strategy.
			foreach (var action in exampleActions) {
				// Notice how by using the key we can treat it similar to how we would call another method.
				strategies[action](exampleActions.IndexOf(action), action);
			}
		}
	}
}